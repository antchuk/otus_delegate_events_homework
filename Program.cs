﻿using System.Collections;
using System.Runtime.CompilerServices;
using System.Threading;

namespace homework6
{
    internal class Program
    {
		static void DisplayMessage(FileArgs message) => Console.WriteLine("{0,15} File name {1,20}", DateTime.Now.ToString("ss:ffff"), message.Name);

        static void GetMaxTest()
        {
			var test_data = new List<GetMaxExtention.TestData>()
			{
				new GetMaxExtention.TestData(1,222),
				new GetMaxExtention.TestData(2,22),
				new GetMaxExtention.TestData(3,1),
				new GetMaxExtention.TestData(4, 0)
			};
			Console.WriteLine("Max value of dataset = {0}",test_data.GetMax<GetMaxExtention.TestData>((x) => x.ValueTest).ValueTest.ToString());			
        }
        static void FileFinderTest()
        {
			CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
			CancellationToken cancellationToken = cancellationTokenSource.Token;
			var FileS = new FileFinder(Directory.GetCurrentDirectory() + "\\data\\");
			FileS.FileFound += DisplayMessage;
			FileS.StartSearch(cancellationToken);
			Console.WriteLine("Wait ...");
			Task.Delay(500).Wait();			
			cancellationTokenSource.Cancel();
			Console.WriteLine("Cancel ...");
		}
		static void Main(string[] args)
        {            
            Console.WriteLine("Hello, World!");
			FileFinderTest();
			//Console.ReadKey();
			GetMaxTest();
			Console.ReadKey();
		}
		
	}
}