﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework6
{
    /// <summary>
    /// класс для перебора файлов в диреткории
    /// </summary>
    public class FileFinder
    {
        private readonly string _dirpath;
		public delegate void FileFoundHadler(FileArgs e);
        /// <summary>
        /// событие при нахождении файла в указанной директории
        /// </summary>
		public event FileFoundHadler? FileFound;
		/// <summary>
		/// класс для перебора файлов в диреткории
		/// </summary>
		/// <param name="path"></param>
		public FileFinder (string path)
        {
            _dirpath = path;
            if (!Directory.Exists(_dirpath)) 
            {
                Directory.CreateDirectory(_dirpath);
            }
        }
        /// <summary>
        /// начать поиск файлов
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task StartSearch(CancellationToken token = default)
        {
            Task.Run(() => { Search(token); },token);
            return Task.CompletedTask;
        }
        private void Search( CancellationToken token)
        {
			if (token.IsCancellationRequested)
			{
				return;
			}
			var files = Directory.GetFiles(_dirpath);
			foreach (var file in files)
			{
				if (token.IsCancellationRequested)
				{
					break;
				}
				string _filename = new FileInfo(file).Name;

				FileFound?.Invoke(new FileArgs() { Name = _filename });
			}
		}
    }
    /// <summary>
    /// 
    /// </summary>
    public class FileArgs : EventArgs
    {      
        public string Name { get; set; }
    }
}
