﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework6
{
	internal static class GetMaxExtention
	{
		public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
		{
			if (e == null) throw new ArgumentNullException(nameof(e));
			T? max = null;
			foreach (var item in e) 
			{
				if (max == null || (getParameter((T)item)> getParameter(max)))
				{
					max = (T)item;
				}
			}
			return max;
		}
		public class TestData
		{
			public TestData(int i, float f)
			{
				if (i == null)
					IndexTest = 0;
				else
				{
					IndexTest = i;
				}

				if (f == null)
					ValueTest = 0;
				else
				{
					ValueTest = f;
				}
			}
			public int IndexTest { get; set; }
			public float ValueTest {  get; set; }
		}
	}
}
